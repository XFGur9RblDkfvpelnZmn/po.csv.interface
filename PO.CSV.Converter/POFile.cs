﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace PO.CSV.Converter
{
    public class POFile
    {
        static string sDelimiter = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
        static List<string> lsLanguages = new List<string>();
        public string sLang = string.Empty;
        Dictionary<string, string> dssLine = new Dictionary<string, string>();

        /// <summary>
        /// create POFile from .properties file
        /// </summary>
        /// <param name="fiFile"></param>
        public POFile(FileInfo fiFile)
        {
            if (fiFile.Exists)
            {
                string sContent = string.Empty;
                using (StreamReader sr = new StreamReader(fiFile.FullName,Encoding.GetEncoding("Windows-1252"),true))
                {
                    sContent = sr.ReadToEnd();
                }
                sContent = sContent.Replace("\r",string.Empty);
                string[] sLine = sContent.Split('\n');
                for (int i=0; i < sLine.Length;i++)
                {
                    if (sLine[i].Length > 0 && sLine[i].Substring(0, 1) != "#" && sLine[i].Substring(0, 1) != "\\" && sLine[i].Contains('='))
                    {
                        string[] sData = sLine[i].Split('=');
                        if (sData.Length >= 2 && !dssLine.ContainsKey(sData[0].Trim()))
                        {
                            string sAddText = string.Empty;
                            string sKey = sData[0].Trim();
                            string sTempText = sData[1].Trim();
                            do
                            {
                                sAddText += sTempText;
                                i++;
                                if(i<sLine.Length)
                                    sTempText = sLine[i].Trim();
                            }
                            while (sAddText.Length > 0 && sAddText.Substring(sAddText.Length - 1) == "\\");
                            i--;
                            dssLine.Add(sKey, sAddText);
                        }
                    }
                }
            }
            else
            {
                throw new FileNotFoundException("File not found", fiFile.FullName);
            }
        }

        /// <summary>
        /// Create POFile from dictionary
        /// </summary>
        /// <param name="dssLine"></param>
        /// <param name="sLang"></param>
        public POFile(Dictionary<string,string> dssLine, string sLang)
        {
            this.dssLine = dssLine;
            this.sLang = sLang;
        }

        /// <summary>
        /// Convert POFile object to CSV
        /// </summary>
        /// <param name="fiFile">Output CSV file</param>
        /// <param name="sLanguages">List of languages to include in csv file</param>
        public void ToCSV(FileInfo fiFile,string[] sLanguages)
        {
            
            using (StreamWriter sw = new StreamWriter(fiFile.FullName))
            {
                sw.Write("Property");
                foreach (string sLang in sLanguages)
                {
                    sw.Write(sDelimiter+sLang);
                }
                sw.Write(Environment.NewLine);
                foreach (KeyValuePair<string, string> kvp in dssLine)
                {

                    sw.Write("\""+kvp.Key + "\""+sDelimiter+"\"" + kvp.Value+"\"");
                    foreach (string sLang in sLanguages)
                    {
                        sw.Write(sDelimiter);
                    }
                    sw.Write(Environment.NewLine);
                }
            }
        }

        /// <summary>
        /// Convert POFile object to .properties file
        /// </summary>
        /// <param name="fiFile">output .properties files</param>
        public void ToPOFile(FileInfo fiFile)
        {
            using (StreamWriter sw = new StreamWriter(fiFile.FullName))
            {
                List<string> lsKeys = new List<string>();
                foreach (KeyValuePair<string, string> kvp in dssLine)
                {
                    lsKeys.Add(kvp.Key);
                }
                foreach (string key in lsKeys)
                {
                    sw.Write(key.Replace("\"",string.Empty) + "=" + dssLine[key].Split('\\')[0].Replace("\"",string.Empty));
                    do
                    {
                        if (dssLine[key].Split('\\').Length > 1)
                        {
                            sw.Write("\\\n\t\t\t\t\t");
                            dssLine[key] = dssLine[key].Substring(dssLine[key].IndexOf('\\') + 1);
                            sw.Write(dssLine[key].Split('\\')[0].Replace("\"", string.Empty));
                        }
                            
                        
                    }
                    while (dssLine[key].Contains('\\'));
                    sw.Write("\n");
                }
            }
        }

        /// <summary>
        /// Convert csv file to List of POFile objects (per language)
        /// </summary>
        /// <param name="fiFile">Input file</param>
        /// <returns></returns>
        public static List<POFile> ParseCsv(FileInfo fiFile)
        {
            List<POFile> lpoReturn = new List<POFile>();
            if (fiFile.Exists)
            {
                string sContent = string.Empty;
                
                using (StreamReader sr = new StreamReader(fiFile.FullName,Encoding.GetEncoding("Windows-1252"),true))
                {
                    sContent = sr.ReadToEnd();
                }
                Match match = Regex.Match(sContent, @""".*?""", RegexOptions.Singleline);
                if (match.Success)
                {
                    sContent = sContent.Replace(match.Value, match.Value.Replace("\n", "\\"));
                }
                sContent = sContent.Replace("\r", string.Empty);
                string[] sLine = sContent.Split('\n');
                string[] sLang = Regex.Split(sLine[0], sDelimiter);

                if (sLang.Length >= 2)
                {
                    Dictionary<int,string> disLanguages = new Dictionary<int,string>();
                    for (int i = 1; i < sLang.Length; i++)
                    {
                        disLanguages.Add(i,sLang[i]);
                    }

                    foreach (KeyValuePair<int,string> kvp in disLanguages)
                    {
                        Dictionary<string, string> dss = new Dictionary<string, string>();
                        for (int i = 1; i < sLine.Length; i++)
                        {

                            string sRegex = sDelimiter+"(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"; 
                            string[] sLineData = Regex.Split(sLine[i], sRegex);
                            if (!dss.ContainsKey(sLineData[0].Trim()))
                            {
                                if (sLineData.Length > kvp.Key)
                                {
                                    dss.Add(sLineData[0], sLineData[kvp.Key]);
                                }
                            }
                            
                        }
                        POFile pof = new POFile(dss, kvp.Value);
                        lpoReturn.Add(pof);
                    }
                }
            }
            return lpoReturn;
        }

        
    }
}
