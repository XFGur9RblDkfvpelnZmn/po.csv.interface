﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using PO.CSV.Converter;
namespace PO.CSV.ConverterGui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowseInput_Click(object sender, EventArgs e)
        {
            DialogResult result = this.openFileDialog.ShowDialog();
            // if a file is selected
            if (result == DialogResult.OK)
            {
                // Set the selected file URL to the textbox
                this.txtInputFile.Text = this.openFileDialog.FileName;
                this.txtOutputFile.Text = string.Empty;
                FileInfo fi = new FileInfo(this.openFileDialog.FileName);
                if (fi.Extension.ToLower() == ".csv")
                {
                    this.saveFileDialog.Filter = "Properties|*.properties";
                    txtLang.Enabled = false;
                    lblLanguage.Enabled = false;
                }
                else
                {
                    this.saveFileDialog.Filter = "CSV Files|*.csv";
                    txtLang.Enabled = true;
                    lblLanguage.Enabled = true;
                }
                this.lblOutputFile.Enabled = true;
                
                this.btnBrowseOutput.Enabled = true;
                this.button1.Enabled = false;
            }
            else
            {
                try
                {
                    FileInfo fi = new FileInfo(this.txtInputFile.Text);
                    if (fi.Extension.ToLower() == ".csv" || fi.Extension.ToLower() == ".properties")
                    {
                        this.lblOutputFile.Enabled = true;
                        this.btnBrowseOutput.Enabled = true;
                        this.button1.Enabled = false;
                    }
                    else
                    {
                        this.lblOutputFile.Enabled = false;
                        this.btnBrowseOutput.Enabled = false;
                        this.button1.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void btnBrowseOutput_Click(object sender, EventArgs e)
        {
            DialogResult result = this.saveFileDialog.ShowDialog();
            // if a file is selected
            if (result == DialogResult.OK)
            {
                // Set the selected file URL to the textbox
                this.txtOutputFile.Text = this.saveFileDialog.FileName;
                FileInfo fi = new FileInfo(this.saveFileDialog.FileName);
                this.button1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                string[] lsLang = txtLang.Text.Split(';');
                FileInfo fiInput = new FileInfo(this.openFileDialog.FileName);
                FileInfo fiOutput = new FileInfo(this.saveFileDialog.FileName);

                if (fiInput.Extension.ToLower() == ".properties")
                {
                    POFile po = new POFile(fiInput);
                    po.ToCSV(fiOutput, lsLang);
                }
                else
                {
                    List<POFile> lpof = POFile.ParseCsv(fiInput);
                    foreach (POFile po in lpof)
                    {
                        string sOutputMap = fiOutput.Directory.ToString() + "\\";
                        string sOutputFile = Path.GetFileNameWithoutExtension(fiOutput.Name);
                        string sOutputExtension = fiOutput.Extension;
                        po.ToPOFile(new FileInfo(sOutputMap + sOutputFile + po.sLang + sOutputExtension));
                    }
                }
                MessageBox.Show("Convert successful");
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Input file not found");
            }
            catch (IOException ex)
            {
                MessageBox.Show("Could not edit outputfile (make sure the file isn't opened in Excel)");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Input and/or output file paths are not configured correctly");
            }
            
        }

       



    }
}
